## API REST Prueba tecnica ingeneo <br>
participante : Josue Henriquez

------------

####Tecnologia utilizada

* Java (version 8)
* Proyecto Maven
* Base de datos: PostgreSQL.
* framewor: spring boot version 2.6.3
* documentacion de endpoint : swagger
* despliegue: Heroku url: https://tranquil-thicket-58562.herokuapp.com/

#### Listado de EndPoint's
* /api/auth/signup ; creacion de usuarios con listado de roles: admin, usr y cliente
* /api/auth/login : autenticación de los usuarios (Bearer Token).
* /crud/customer : gestion de clientes, rol de acceso : ADMIN
* /crud/product: gestion de catalogos de productos, rol de acceso : ADMIN.
* /crud/warehouse: gestion de depositos (bodegas y puertos), rol de acceso : ADMIN.
* /crud/service: catálogo de tipo de servicio (1 : logistica terrestre, 2:logistica maritima ), rol de acceso ADMIN.
* /warehouses, administración de puertos y/o bodegas.
* /deliverie, gestion para los planes de entrega.

Nota: para todos los endpoints crud, se crearon los mismos 5 tipos de operacion

* /all = Método HTTP: GET : returna un listado de la entidad
* / = Método HTTP: POST : crea la entidad
* /{id} = Método HTTP: GET : devuelve una entidad en base a su id
* /{id} =Método HTTP: PUT: actualiza una entidad
* / = Método HTTP: DELETE : elimina una entidad.


####Base de datos (Diagrama E-R)
![Diagrama ER](ER-Diagrama.png)


####Seguridad <br>

Autenticacion ejemplo:

curl --location --request POST 'https://tranquil-thicket-58562.herokuapp.com/api/auth/login' \
--header 'Content-Type: application/json' \
--data-raw '{
"username":"admin.logistica",
"password":"123456"
}'

Ejemplo de uso de token:

curl --location --request POST 'https://tranquil-thicket-58562.herokuapp.com/crud/customer/' \
--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbi5sb2dpc3RpY2EiLCJpYXQiOjE2NDU0MTM3MjEsImV4cCI6MTY0NTUwMDEyMX0.0lCIUiDhp8q6DQv1sJOF2CKhBSSLhzoSC0E7GoLnA9Y8VIyiwrCEvTtFn-HFRAb7138tvnG1Jhd4bHJasLVjnA' \
--header 'Content-Type: application/json' \
--data-raw '{
"active": true,
"address": "col1",
"created": "2022-02-20T21:00:12.398Z",
"email": "juan.perez@gmail.com",
"name": "Juan Perez",
"phone": "22548495"
}'


